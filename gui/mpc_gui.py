""" For GUI Test file classify """

import sys
import time
from tkinter import Button, Label, PhotoImage, Tk

from preliminary_round_db_info import GetUserInfo


class MazeDisplay:
    """ class to all display"""

    GROUP = [
        "FRESH MAN",
        "EXPERT-STEP Preliminary",
        "EXPERT-DC Preliminary",
        "TIME ATTACK",
        "MICROMOUSE",
        "EXPERT-STEP Fianl",
        "EXPERT-DC Fianl",
    ]
    VAL = {
        "run_begin": time.time(),
        "left_begin": time.time(),
        "count": 0,
        "sec_count": 0,
        "left_time": 300,
        "line": 0,
        "last": 0,
    }

    def __init__(self) -> None:

        self.__root = Tk()
        self.__num_data = {
            "font_size": 18,
            "x": 10,
            "y": 10,
            "row": 10,
            "column": 10,
            "pos": "w",
        }
        self.__text_data = {
            "back_color": "white",
            "font": "맑은고딕",
            "main_title": "MAZE_TIMER",
            "pos_x": 0,
            "pos_y": 0,
            "font_color": "black",
        }
        self.__resol = {
            "resolution": "1920x1080",
            "x": 1920 / 1920,
            "y": 1080 / 1080,
        }
        self.__logo = {
            "dankook":
            PhotoImage(file="./image/dankook_logo.png").subsample(
                int(3 / self.__resol["x"]), int(2 / self.__resol["y"])),
            "center":
            PhotoImage(file="./image/center_logo.png").subsample(
                int(3 / self.__resol["x"]), int(2 / self.__resol["y"])),
            "maze":
            PhotoImage(file="./image/maze_logo.png").subsample(
                int(3 / self.__resol["x"]), int(2 / self.__resol["y"])),
            "mark":
            PhotoImage(file="./image/dku_mark.png").subsample(
                int(4 / self.__resol["x"]), int(4 / self.__resol["y"])),
        }

    def __del__(self) -> None:
        del self.__root

    ##########################################################################
    # Public methods
    def main_loop(self) -> None:
        """ mainloop """

        self.__root.mainloop()

    def run_gui(self) -> None:
        """ run gui system """

        self.__fix_data()
        self.__update_sec()
        self.__update_mil()

    ##########################################################################
    # Internal methods
    def __fix_data(self) -> None:
        """ fixed data """

        self.__root.title(self.__text_data["main_title"])
        self.__root.geometry(self.__resol["resolution"])
        self.__root.configure(background=self.__text_data["back_color"])

        self.__image(self.__logo["mark"], 1, 1)
        self.__image(self.__logo["dankook"], 1330, 930)
        self.__image(self.__logo["center"], 1500, 930)
        self.__image(self.__logo["maze"], 1670, 970)

        self.__num_data.update(x=45, y=270, font_size=18, row=15, column=1)
        self.__text("Left Contest Time", self.__text_data["font_color"])
        self.__num_data.update(x=60, y=440)
        self.__text("Driving Time", self.__text_data["font_color"])
        self.__num_data.update(y=580)
        self.__text("Driving Record", self.__text_data["font_color"])
        self.__num_data.update(y=700)
        self.__text("Player Ranking", self.__text_data["font_color"])
        self.__num_data.update(x=1520, y=40)
        self.__text("Contest Player List", self.__text_data["font_color"])

        __quit = Button(text="quit", command=self.__root.destroy)
        __quit.pack(side="left", anchor="s")

    def __update_sec(self) -> None:
        """ update data 1 sec period """

        info = GetUserInfo(sys.argv[1], sys.argv[2])
        print(info.first_group())
        __current_time = time.strftime("%a %b %d  %I:%M %p")

        self.__num_data.update(x=1600, y=1, font_size=20, row=20, colmn=1)
        self.__text(str(__current_time), "purple")

        self.__num_data.update(x=700, font_size=30, row=20, column=1)
        self.__text(MazeDisplay.GROUP[MazeDisplay.VAL["count"]], "blue")

        self.__root.after(1234, self.__update_sec)

    def __update_mil(self) -> None:
        """ update data mili sec period """

        self.__falling_time()
        self.__rising_time()

    def __rising_time(self) -> None:
        """ rising timer (stop watch) """

        __running_time = time.time() - MazeDisplay.VAL["run_begin"]

        __run_sum = cal_time(__running_time)

        self.__num_data.update(x=330, y=380, font_size=80, row=20, column=1)
        self.__text(__run_sum, "black")

        self.__root.after(123, self.__rising_time)

    def __falling_time(self) -> None:
        """ falling timer (time_counter) """

        if MazeDisplay.VAL["count"] == 0:
            MazeDisplay.VAL["left_begin"] = time.time()
            MazeDisplay.VAL["count"] = 1

        __left_time = 300 - (time.time() - MazeDisplay.VAL["left_begin"])

        if MazeDisplay.VAL["count"] == 0:
            __left_time = 300
        if __left_time < 0:
            __left_time = 0

        __left_sum = cal_time(__left_time)

        self.__num_data.update(x=330, y=200, font_size=80, row=20, column=1)
        self.__text(__left_sum, "black")

        self.__root.after(155, self.__falling_time)

    def __image(self, image_data, pos_x: int, pos_y: int) -> None:
        """ display image data """

        __img = Label(
            self.__root,
            image=image_data,
            width=int(170 * self.__resol["x"]),
            height=int(120 * self.__resol["y"]),
            bg=self.__text_data["back_color"],
        )

        self.__text_data["pos_x"] = pos_x * self.__resol["x"]
        self.__text_data["pos_y"] = pos_y * self.__resol["y"]
        __img.place(x=self.__text_data["pos_x"], y=self.__text_data["pos_y"])

    def __text(self, name: str, font_color: str) -> None:
        """ display text data """

        __label = Label(
            self.__root,
            text=name,
            width=self.__num_data["row"],
            height=self.__num_data["column"],
            bg=self.__text_data["back_color"],
            font=(
                self.__text_data["font"],
                int(self.__num_data["font_size"] * self.__resol["x"]),
            ),
            fg=font_color,
        )

        self.__text_data["pos_x"] = self.__num_data["x"] * self.__resol["x"]
        self.__text_data["pos_y"] = self.__num_data["y"] * self.__resol["y"]
        __label.place(x=self.__text_data["pos_x"], y=self.__text_data["pos_y"])


def cal_time(full_time: int) -> str:
    """ calculate time for stop watch / time counter """

    run_min = int(full_time / 60)
    run_mil = float(format((full_time % 60), ".3f"))
    run_sec = int(run_mil)
    run_mil = int(float(format((run_mil - run_sec), ".3f")) * 1000)

    str_sec = str(run_sec)
    if run_sec == 0:
        str_sec = "00"
    elif run_sec < 10:
        str_sec = "0" + str(run_sec)

    str_mil = str(run_mil)
    if run_mil == 0:
        str_mil = "000"
    elif run_mil < 10:
        str_mil = "00" + str(run_mil)
    elif run_mil < 100:
        str_mil = "0" + str(run_mil)

    run_sum = str(run_min) + ":" + str_sec + ":" + str_mil

    return run_sum


def main():
    """ main func """

    maze = MazeDisplay()
    maze.run_gui()
    maze.main_loop()


if __name__ == "__main__":
    sys.exit(main() or 0)
