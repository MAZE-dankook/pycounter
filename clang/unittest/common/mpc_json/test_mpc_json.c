#include <errno.h>
#include <pthread.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <cmocka.h>
#include <json.h>

#include <pycounter/common/mpc_common.h>
#include <pycounter/common/mpc_json.h>

//#define DEBUG_MODE

#define TEST_THREAD_NUM 4

struct test_data
{
    const char *id;
    const char *key;
    const char *data;

    int idx;
    int len;

    sync_t *cur_sync;
    sync_t *next_sync;
};

static inline int _test_data_init(struct test_data *td, const char *id,
                                  const char *key, const char *data,
                                  const int idx, const int len);
static inline sync_t *_sync_init(void);
static inline int _sync_exit(sync_t *sync);

static void *_thread_send_data(void *arg)
{
    struct test_data *td = (struct test_data *)arg;
    sync_t *cur_sync = td->cur_sync;
    sync_t *next_sync = td->next_sync;

    int cnt = 0;
    char *ret = NULL;
    char *respond = calloc(1, td->len + 1);

    pthread_mutex_lock(&cur_sync->lock);
    pthread_cond_signal(&cur_sync->cv);
    pthread_mutex_unlock(&cur_sync->lock);

    while (cnt++ < 100)
    {
        pthread_mutex_lock(&cur_sync->lock);
        pthread_cond_wait(&cur_sync->cv, &cur_sync->lock);
        pthread_mutex_unlock(&cur_sync->lock);

        pthread_mutex_lock(&cur_sync->lock);
        ret = (char *)mpc_json_send_data(td->id, td->key, td->data);
#ifdef DEBUG_MODE
        printf("Thread %d: %3d %s\n", td->idx, cnt, ret);
#endif
        /* check the return data length */
        assert_int_equal((int)strlen(ret), td->len);
        if (cnt == 100)
            snprintf(respond, td->len + 1, "%s", ret);

        /* Have to change the thread */
        sleep(0);

        pthread_cond_signal(&next_sync->cv);
        pthread_mutex_unlock(&cur_sync->lock);
    }

    return respond;
}

static void _test_mpc_json_send_data(void **state)
{
    FORMULA_GUARD(state == NULL || *state == NULL, , ERR_INVALID_PTR);

    pthread_t *thread = malloc(sizeof(pthread_t) * TEST_THREAD_NUM);
    FORMULA_GUARD(thread == NULL, , ERR_MEMORY_SHORTAGE);

    int i;
    int ret = -EPERM;
    sync_t *sync = NULL;
    struct test_data *td = *state;
    for (i = 0; i < TEST_THREAD_NUM; i++)
    {
        sync = td[i].cur_sync;
        pthread_mutex_lock(&sync->lock);
        ret = pthread_create(&thread[i], NULL, _thread_send_data, &td[i]);
        if (ret < 0)
        {
            ERR_MSG("Failed to create the pthread.");
            pthread_mutex_unlock(&sync->lock);
            return;
        }
        pthread_cond_wait(&sync->cv, &sync->lock);
        pthread_mutex_unlock(&sync->lock);
    }

    /* start first thread */
    pthread_cond_signal(&td->cur_sync->cv);

    char *th_ret = NULL;
    const char *json[] = {
        "{ \"id\": \"Timer\", \"key\": \"Current Record\", "
        "\"data\": \"xxxx:xx:xx:xx\" }",
        "{ \"id\": \"System\", \"key\": \"Previous Player\", \"data\": "
        "\"NULL\" }",
        "{ \"id\": \"Timer\", \"key\": \"Total Remain Lap "
        "Time\", \"data\": \"1234:56:78:90\" }",
        "{ \"id\": \"System\", \"key\": \"Next Player\", \"data\": \"NULL\" }"};
    for (i = 0; i < TEST_THREAD_NUM; i++)
    {
        pthread_join(thread[i], (void **)&th_ret);

        /* check the return data */
        assert_string_equal(json[i], th_ret);
        PTR_FREE(th_ret);
    }

    PTR_FREE(thread);
}

int setup(void **state)
{
    struct test_data *td = malloc(sizeof(struct test_data) * TEST_THREAD_NUM);
    FORMULA_GUARD(td == NULL, -EPERM, ERR_INVALID_PTR);

    int ret = -EPERM;

    ret = _test_data_init(&td[0], "Timer", "Current Record", "xxxx:xx:xx:xx", 0,
                          67);
    FORMULA_GUARD(ret < 0, -EPERM, ERR_INVALID_RET);

    ret = _test_data_init(&td[1], "System", "Previous Player", NULL, 1, 60);
    FORMULA_GUARD(ret < 0, -EPERM, ERR_INVALID_RET);

    ret = _test_data_init(&td[2], "Timer", "Total Remain Lap Time",
                          "1234:56:78:90", 2, 74);
    FORMULA_GUARD(ret < 0, -EPERM, ERR_INVALID_RET);

    ret = _test_data_init(&td[3], "System", "Next Player", NULL, 3, 56);
    FORMULA_GUARD(ret < 0, -EPERM, ERR_INVALID_RET);

    td[0].next_sync = td[1].cur_sync;
    td[1].next_sync = td[2].cur_sync;
    td[2].next_sync = td[3].cur_sync;
    td[3].next_sync = td[0].cur_sync;

    *state = td;

    mpc_json_init();

    return 0;
}

int teardown(void **state)
{
    FORMULA_GUARD(state == NULL || *state == NULL, -EPERM, ERR_INVALID_PTR);

    mpc_json_exit();

    struct test_data *td = *state;

    _sync_exit(td[0].cur_sync);
    _sync_exit(td[1].cur_sync);
    _sync_exit(td[2].cur_sync);
    _sync_exit(td[3].cur_sync);

    PTR_FREE(*state);

    return 0;
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(_test_mpc_json_send_data)};

    int count_fail_tests = cmocka_run_group_tests(tests, setup, teardown);

    return count_fail_tests;
}

static inline int _test_data_init(struct test_data *td, const char *id,
                                  const char *key, const char *data,
                                  const int idx, const int len)
{
    td->cur_sync = _sync_init();
    FORMULA_GUARD(td->cur_sync == NULL, -EPERM, ERR_INVALID_PTR);

    td->id = id;
    td->key = key;
    td->data = data;
    td->idx = idx;
    td->len = len;

    return 0;
}

static inline sync_t *_sync_init(void)
{
    sync_t *sync = malloc(sizeof(sync_t));
    FORMULA_GUARD(sync == NULL, NULL, ERR_INVALID_PTR);

    pthread_cond_init(&sync->cv, NULL);
    pthread_mutex_init(&sync->lock, NULL);

    return sync;
}

static inline int _sync_exit(sync_t *sync)
{
    pthread_cond_destroy(&sync->cv);
    pthread_mutex_destroy(&sync->lock);

    PTR_FREE(sync);

    return 0;
}
