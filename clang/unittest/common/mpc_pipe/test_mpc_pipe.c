#include <errno.h>
#include <pthread.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <cmocka.h>

#include <pycounter/common/mpc_common.h>
#include <pycounter/common/mpc_pipe.h>

#define TEST_THREAD_NUM 2
#define TEST_STR "Unit Test for common!"

struct test_data
{
    sync_t sync;

    void *(*handler[2])(void *arg);
    int (*pipe_func[6])(char *data);
};

static inline void _test_pipe_check(const char *pipe_path,
                                    const char *pipe_name);

static void *_thread_read_pipe(void *arg)
{
    struct test_data *td = (struct test_data *)arg;
    sync_t *sync = &td->sync;

    pthread_mutex_lock(&sync->lock);
    char buf[MAX_BUF] = {
        0,
    };
    pthread_cond_signal(&sync->cv);
    pthread_mutex_unlock(&sync->lock);

    for (int i = 0; i < 3; i++)
    {
        td->pipe_func[i << 1](buf);
        assert_string_equal(buf, TEST_STR);
        printf("Read Data: %s\n", buf);

        /* Have to change the thread */
        sleep(0);

        pthread_mutex_lock(&sync->lock);
        memset(buf, 0x0, sizeof(buf));
        pthread_cond_signal(&sync->cv);
        pthread_mutex_unlock(&sync->lock);
    }

    return NULL;
}

static void *_thread_write_pipe(void *arg)
{
    struct test_data *td = (struct test_data *)arg;
    sync_t *sync = &td->sync;

    pthread_mutex_lock(&sync->lock);
    pthread_cond_signal(&sync->cv);
    pthread_mutex_unlock(&sync->lock);

    for (int i = 0; i < 3; i++)
    {
        printf("Write Data: %s\n", TEST_STR);
        assert_int_equal(td->pipe_func[(i << 1) + 1](TEST_STR),
                         strlen(TEST_STR));

        pthread_mutex_lock(&sync->lock);
        pthread_cond_wait(&sync->cv, &sync->lock);
        pthread_mutex_unlock(&sync->lock);
    }

    return NULL;
}

static void _test_mpc_pipe_rw(void **state)
{
    FORMULA_GUARD(state == NULL || *state == NULL, , ERR_INVALID_PTR);

    pthread_t *thread = malloc(sizeof(pthread_t) * TEST_THREAD_NUM);
    FORMULA_GUARD(thread == NULL, , ERR_INVALID_PTR);

    int ret = -EPERM;
    struct test_data *td = (struct test_data *)*state;
    for (int i = 0; i < TEST_THREAD_NUM; i++)
    {
        pthread_mutex_lock(&td->sync.lock);
        ret = pthread_create(&thread[i], NULL, td->handler[i], td);
        if (ret < 0)
        {
            ERR_MSG("Failed to create the pthread.");
            pthread_mutex_unlock(&td->sync.lock);
            return;
        }
        pthread_cond_wait(&td->sync.cv, &td->sync.lock);
        pthread_mutex_unlock(&td->sync.lock);
    }

    pthread_join(thread[1], NULL);
    pthread_join(thread[0], NULL);

    PTR_FREE(thread);
}

static void _test_mpc_pipe_key_to_timer_rw(void **state)
{
    (void)state;

    /* input the NULL parameter */
    assert_int_equal(mpc_pipe_key_to_timer_read(NULL), -EPERM);
    assert_int_equal(mpc_pipe_key_to_timer_write(NULL), -EPERM);

    /* check the created pipe */
    const char *path = mpc_pipe_get_key_to_timer();
    assert_non_null(path);
    _test_pipe_check(path, "pipe_key_to_timer");
}

static void _test_mpc_pipe_key_to_db_rw(void **state)
{
    (void)state;

    /* input the NULL parameter */
    assert_int_equal(mpc_pipe_key_to_db_read(NULL), -EPERM);
    assert_int_equal(mpc_pipe_key_to_db_write(NULL), -EPERM);

    /* check the created pipe */
    const char *path = mpc_pipe_get_key_to_db();
    assert_non_null(path);
    _test_pipe_check(path, "pipe_key_to_db");
}

static void _test_mpc_pipe_timer_to_db_rw(void **state)
{
    (void)state;

    /* input the NULL parameter */
    assert_int_equal(mpc_pipe_timer_to_db_read(NULL), -EPERM);
    assert_int_equal(mpc_pipe_timer_to_db_write(NULL), -EPERM);

    /* check the created pipe */
    const char *path = mpc_pipe_get_timer_to_db();
    assert_non_null(path);
    _test_pipe_check(path, "pipe_timer_to_db");
}

/* These functions will be used to initialize
   and clean resources up after each test run */
int setup(void **state)
{
    struct test_data *test = calloc(1, sizeof(struct test_data));
    FORMULA_GUARD(test == NULL, -EPERM, ERR_INVALID_PTR);

    pthread_cond_init(&test->sync.cv, NULL);
    pthread_mutex_init(&test->sync.lock, NULL);

    test->handler[0] = _thread_read_pipe;
    test->handler[1] = _thread_write_pipe;

    test->pipe_func[0] = mpc_pipe_key_to_timer_read;
    test->pipe_func[1] = mpc_pipe_key_to_timer_write;
    test->pipe_func[2] = mpc_pipe_key_to_db_read;
    test->pipe_func[3] = mpc_pipe_key_to_db_write;
    test->pipe_func[4] = mpc_pipe_timer_to_db_read;
    test->pipe_func[5] = mpc_pipe_timer_to_db_write;

    *state = test;

    assert_int_equal(mpc_pipe_init(), 0);
    return 0;
}

int teardown(void **state)
{
    assert_int_equal(mpc_pipe_exit(), 0);
    PTR_FREE(*state);
    return 0;
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(_test_mpc_pipe_key_to_timer_rw),
        cmocka_unit_test(_test_mpc_pipe_key_to_db_rw),
        cmocka_unit_test(_test_mpc_pipe_timer_to_db_rw),
        cmocka_unit_test(_test_mpc_pipe_rw)};

    /* If setup and teardown functions are not
       needed, then NULL may be passed instead */

    int count_fail_tests = cmocka_run_group_tests(tests, setup, teardown);

    return count_fail_tests;
}

static inline void _test_pipe_check(const char *pipe_path,
                                    const char *pipe_name)
{
    assert_int_equal(access(pipe_path, F_OK), 0);
    assert_non_null(strstr(pipe_path, pipe_name));
}
