#include <pycounter/common/mpc_common.h>

static int mpc_timer_create();
static int mpc_timer_destroy();

int mpc_timer_init(void)
{
    mpc_timer_create();
    mpc_timer_create();
    return 0;
}
int mpc_timer_exit(void)
{
    mpc_timer_destroy();
    mpc_timer_destroy();
    return 0;
}

int mpc_timer_create() { return 0; }
int mpc_timer_destroy() { return 0; }
