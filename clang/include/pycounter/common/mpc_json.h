/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:ffs=unix */
#ifndef __MPC_JSON_H__
#define __MPC_JSON_H__

#include <pycounter/common/mpc_common.h>

int mpc_json_init(void);
int mpc_json_exit(void);

const char *mpc_json_send_data(const char *id, const char *key,
                               const char *send_data);

#endif /* __MPC_JSON_H__ */
