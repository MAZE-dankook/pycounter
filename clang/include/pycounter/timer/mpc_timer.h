#ifndef __MPC_TIMER_H__
#define __MPC_TIMER_H__

int mpc_timer_init(void);
int mpc_timer_exit(void);

#endif
