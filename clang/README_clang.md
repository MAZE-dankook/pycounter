# How to use the clang?


## For build

* Using the bash script

```bash
$ ./build.sh
```

* Manual build

```bash
$ rm -rf out && mkdir -p out && cd out
$ cmake ../ && make
```


## APIs(extern functions)

* **mpc_main_init**

  * Call the function when starting the process

* **mpc_main_exit**

  * Call the function when ending the process


## How to use it?

* In Python, call the shared library using ***CDLL*** in the ctypes package

* Refer to the example sample code in the Python below,

```bash
import ctypes
import time

if __name__ == "__main__":
    CLIB = ctypes.CDLL("<pycounter root dir>/clang/out/libmpc_shared.so")
    CLIB.mpc_main_init()

    time.sleep(5)

    CLIB.mpc_main_exit()
```
