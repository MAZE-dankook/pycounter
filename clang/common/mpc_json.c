#include <json.h>

#include <pycounter/common/mpc_json.h>

#define JSON_OBJ_SET(object, set_data)                                         \
    do                                                                         \
    {                                                                          \
        int ret;                                                               \
        ret = json_object_set_string(object, set_data);                        \
        FORMULA_GUARD_WITH_EXIT_FUNC(ret == 0, NULL,                           \
                                     pthread_mutex_unlock(&json->lock),        \
                                     ERR_INVALID_RET);                         \
        ret = json_object_object_add(json->respond, #set_data, object);        \
        FORMULA_GUARD_WITH_EXIT_FUNC(ret < 0, NULL,                            \
                                     pthread_mutex_unlock(&json->lock),        \
                                     ERR_INVALID_RET);                         \
        json_object_get(object);                                               \
    } while (0)

typedef struct json_information json_t;

struct json_information
{
    json_object *respond;
    json_object *id;
    json_object *key;
    json_object *data;

    pthread_mutex_t lock;
};

static json_t *_mpc_json_create(void);
static int _mpc_json_destroy(json_t *json);

static json_t *g_json = NULL;

int mpc_json_init(void)
{
    mpc_json_exit();

    g_json = _mpc_json_create();
    FORMULA_GUARD(g_json == NULL, -EPERM, ERR_INVALID_PTR);

    return 0;
}

int mpc_json_exit(void)
{
    FORMULA_GUARD(g_json == NULL, -EPERM, ERR_INVALID_PTR);

    _mpc_json_destroy(g_json);
    g_json = NULL;

    return 0;
}

const char *mpc_json_send_data(const char *id, const char *key,
                               const char *send_data)
{
    json_t *json = g_json;
    FORMULA_GUARD(json == NULL || json->respond == NULL, NULL, ERR_INVALID_PTR);
    FORMULA_GUARD(id == NULL || key == NULL, NULL, ERR_INVALID_PTR);

    pthread_mutex_lock(&json->lock);

    /* set string and add data to respond */
    JSON_OBJ_SET(json->id, id);
    JSON_OBJ_SET(json->key, key);

    const char *data = send_data ? send_data : "NULL";
    JSON_OBJ_SET(json->data, data);

    /* get string as JSON format */
    const char *respond = json_object_get_string(json->respond);

    pthread_mutex_unlock(&json->lock);

    return respond;
}

static json_t *_mpc_json_create(void)
{
    json_t *json = malloc(sizeof(json_t));
    FORMULA_GUARD_WITH_EXIT_FUNC(json == NULL, NULL, _mpc_json_destroy(json),
                                 ERR_INVALID_PTR);

    pthread_mutex_init(&json->lock, NULL);

    /* memory allocate for json object */
    json->respond = json_object_new_object();
    json->id = json_object_new_string("id value on the timer or system");
    json->key = json_object_new_string("key value on the timer or system");
    json->data = json_object_new_string("data value from the timer");
    FORMULA_GUARD_WITH_EXIT_FUNC((json->respond == NULL || json->id == NULL ||
                                  json->key == NULL || json->data == NULL),
                                 NULL, _mpc_json_destroy(json),
                                 ERR_INVALID_PTR);

    return json;
}

static int _mpc_json_destroy(json_t *json)
{
    FORMULA_GUARD(json == NULL, -EPERM, ERR_INVALID_PTR);

    pthread_mutex_destroy(&json->lock);

    /* memory free for json object */
    json_object_put(json->data);
    json_object_put(json->key);
    json_object_put(json->id);
    json_object_put(json->respond);

    PTR_FREE(json);

    return 0;
}
