#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <pycounter/common/mpc_pipe.h>

typedef struct pipe_information pipe_t;

enum
{
    KEY_TO_TIMER,
    KEY_TO_DB,
    TIMER_TO_DB,
    TOTAL_PIPE
};

struct pipe_information
{
    int fd;
    char *path;
    char buf[MAX_BUF];

    sync_t sync;
};

static int _mpc_pipe_set_pipes_path(char pipes[][MAX_BUF]);

static pipe_t *_mpc_pipe_create(const char *path);
static int _mpc_pipe_destroy(pipe_t *pipe);

static int _mpc_pipe_open(pipe_t *pipe);
static int _mpc_pipe_close(pipe_t *pipe);
static int _mpc_pipe_write(pipe_t *pipe);
static int _mpc_pipe_read(pipe_t *pipe);

static inline int _mpc_pipe_get_base_path(char *out_path);
static inline int _mpc_pipe_check(pipe_t *pipe);
static inline int _mpc_pipe_write_wrapper(pipe_t *pipe, const char *wdata);
static inline int _mpc_pipe_read_wrapper(pipe_t *pipe, char *rdata);
static inline const char *_mpc_pipe_get_pipe_path(pipe_t *pipe);

static pipe_t *g_pipe[TOTAL_PIPE];

int mpc_pipe_init(void)
{
    mpc_pipe_exit();

    char pipes[TOTAL_PIPE][MAX_BUF] = {
        0,
    };
    FORMULA_GUARD(_mpc_pipe_set_pipes_path(pipes) < 0, -EPERM, ERR_INVALID_RET);

    for (int i = 0; i < TOTAL_PIPE; i++)
    {
        g_pipe[i] = _mpc_pipe_create((const char *)pipes[i]);
        FORMULA_GUARD_WITH_EXIT_FUNC(g_pipe[i] == NULL, -EPERM, mpc_pipe_exit(),
                                     ERR_INVALID_RET);

        FORMULA_GUARD_WITH_EXIT_FUNC(_mpc_pipe_open(g_pipe[i]) < 0, -EPERM,
                                     mpc_pipe_exit(), ERR_INVALID_RET);
    }

    return 0;
}

int mpc_pipe_exit(void)
{
    for (int i = 0; i < TOTAL_PIPE; i++)
    {
        _mpc_pipe_close(g_pipe[i]);
        _mpc_pipe_destroy(g_pipe[i]);
    }

    return 0;
}

int mpc_pipe_key_to_timer_write(char *wdata)
{
    return _mpc_pipe_write_wrapper(g_pipe[KEY_TO_TIMER], wdata);
}

int mpc_pipe_key_to_timer_read(char *rdata)
{
    return _mpc_pipe_read_wrapper(g_pipe[KEY_TO_TIMER], rdata);
}

int mpc_pipe_key_to_db_write(char *wdata)
{
    return _mpc_pipe_write_wrapper(g_pipe[KEY_TO_DB], wdata);
}

int mpc_pipe_key_to_db_read(char *rdata)
{
    return _mpc_pipe_read_wrapper(g_pipe[KEY_TO_DB], rdata);
}

int mpc_pipe_timer_to_db_write(char *wdata)
{
    return _mpc_pipe_write_wrapper(g_pipe[TIMER_TO_DB], wdata);
}

int mpc_pipe_timer_to_db_read(char *rdata)
{
    return _mpc_pipe_read_wrapper(g_pipe[TIMER_TO_DB], rdata);
}

const char *mpc_pipe_get_key_to_timer(void)
{
    return _mpc_pipe_get_pipe_path(g_pipe[KEY_TO_TIMER]);
}

const char *mpc_pipe_get_key_to_db(void)
{
    return _mpc_pipe_get_pipe_path(g_pipe[KEY_TO_DB]);
}

const char *mpc_pipe_get_timer_to_db(void)
{
    return _mpc_pipe_get_pipe_path(g_pipe[TIMER_TO_DB]);
}

static int _mpc_pipe_set_pipes_path(char pipes[][MAX_BUF])
{
    char out_path[MAX_BUF] = {
        0,
    };
    FORMULA_GUARD(_mpc_pipe_get_base_path(out_path) < 0, -EPERM, "");

    const char *pipe_name[] = {"pipe_key_to_timer", "pipe_key_to_db",
                               "pipe_timer_to_db"};
    for (int i = 0; i < TOTAL_PIPE; i++)
        snprintf(pipes[i], MAX_BUF, "%s%s", out_path, pipe_name[i]);

    return 0;
}

static pipe_t *_mpc_pipe_create(const char *path)
{
    pipe_t *pipe = calloc(1, sizeof(pipe_t));
    FORMULA_GUARD_WITH_EXIT_FUNC(pipe == NULL, NULL, _mpc_pipe_destroy(pipe),
                                 ERR_INVALID_PTR);

    pipe->path = calloc(1, strlen(path) + 1);
    FORMULA_GUARD_WITH_EXIT_FUNC(pipe->path == NULL, NULL,
                                 _mpc_pipe_destroy(pipe),
                                 "Failed to allocate the memory.");
    snprintf(pipe->path, strlen(path) + 1, "%s", path);

    if (_mpc_pipe_check(pipe) == 0)
        unlink(pipe->path);

    FORMULA_GUARD_WITH_EXIT_FUNC(mkfifo(pipe->path, 0666) < 0, NULL,
                                 _mpc_pipe_destroy(pipe),
                                 "Failed to create the pipe. %s", pipe->path);

    printf("Craete the new pipe %s\n", pipe->path);

    pthread_cond_init(&pipe->sync.cv, NULL);
    pthread_mutex_init(&pipe->sync.lock, NULL);

    return pipe;
}

static int _mpc_pipe_destroy(pipe_t *pipe)
{
    FORMULA_GUARD(pipe == NULL, -EPERM, ERR_INVALID_PTR);

    if (_mpc_pipe_check(pipe) == 0)
        unlink(pipe->path);

    pthread_cond_destroy(&pipe->sync.cv);
    pthread_mutex_destroy(&pipe->sync.lock);

    PTR_FREE(pipe->path);
    PTR_FREE(pipe);

    return 0;
}

static int _mpc_pipe_open(pipe_t *pipe)
{
    pipe->fd = open(pipe->path, O_RDWR | O_SYNC);
    FORMULA_GUARD(pipe->fd < 0, -EPERM, ERR_INVALID_FD);

    return 0;
}

static int _mpc_pipe_close(pipe_t *pipe)
{
    FORMULA_GUARD(pipe == NULL, -EPERM, ERR_INVALID_PTR);
    FORMULA_GUARD(pipe->fd < 0, -EPERM, ERR_INVALID_FD);
    FORMULA_GUARD(_mpc_pipe_check(pipe) < 0, -EPERM, "");

    close(pipe->fd);
    pipe->fd = -EPERM;

    return 0;
}

static int _mpc_pipe_write(pipe_t *pipe)
{
    FORMULA_GUARD(pipe->fd < 0, -EPERM, ERR_INVALID_FD);
    FORMULA_GUARD(_mpc_pipe_check(pipe) < 0, -EPERM, "");

    pthread_mutex_lock(&pipe->sync.lock);
    int ret = write(pipe->fd, pipe->buf, strlen(pipe->buf));
    pthread_cond_signal(&pipe->sync.cv);
    pthread_mutex_unlock(&pipe->sync.lock);

    return ret;
}

static int _mpc_pipe_read(pipe_t *pipe)
{
    FORMULA_GUARD(pipe->fd < 0, -EPERM, ERR_INVALID_FD);
    FORMULA_GUARD(_mpc_pipe_check(pipe) < 0, -EPERM, "");

    pthread_mutex_lock(&pipe->sync.lock);
    pthread_cond_wait(&pipe->sync.cv, &pipe->sync.lock);
    int ret = read(pipe->fd, pipe->buf, sizeof(pipe->buf));
    pthread_mutex_unlock(&pipe->sync.lock);

    return ret;
}

static inline int _mpc_pipe_get_base_path(char *out_path)
{
    /* get absolute path */
    char base_path[MAX_BUF] = {
        0,
    };
    FORMULA_GUARD(getcwd(base_path, sizeof(base_path)) == NULL, -EPERM,
                  ERR_INVALID_PTR);

    /* find the pointer which starting the clang folder */
    char *end = strstr(base_path, "clang/");
    if (end == NULL)
    {
        /* for the gui/ directory */
        end = strstr(base_path, "gui");
        FORMULA_GUARD(end == NULL, -EPERM, ERR_INVALID_PTR);
    }

    /* set the path to the <pycounter>/clang/out */
    snprintf(out_path, sizeof(char) * (end - base_path) + 1, "%s", base_path);
    FORMULA_GUARD(strlen(out_path) + strlen("clang/out/") >= MAX_BUF, -EPERM,
                  "Overflow the file path length!");
    strcat(out_path, "clang/out/");

    return 0;
}

static inline int _mpc_pipe_check(pipe_t *pipe)
{
    FORMULA_GUARD(access(pipe->path, F_OK), -EPERM, "Not exist the pipe. %s",
                  pipe->path);
    return 0;
}

static inline int _mpc_pipe_write_wrapper(pipe_t *pipe, const char *wdata)
{
    FORMULA_GUARD(pipe == NULL, -EPERM, ERR_INVALID_PTR);
    FORMULA_GUARD(wdata == NULL, -EPERM, ERR_INVALID_PTR);
    FORMULA_GUARD(strlen(wdata) == 0, 0, "");

    snprintf(pipe->buf, sizeof(pipe->buf), "%s", wdata);
    return _mpc_pipe_write(pipe);
}

static inline int _mpc_pipe_read_wrapper(pipe_t *pipe, char *rdata)
{
    FORMULA_GUARD(pipe == NULL, -EPERM, ERR_INVALID_PTR);
    FORMULA_GUARD(rdata == NULL, -EPERM, ERR_INVALID_PTR);

    int ret = _mpc_pipe_read(pipe);
    FORMULA_GUARD(ret < 0, -EPERM, "");

    snprintf(rdata, sizeof(pipe->buf), "%s", pipe->buf);
    return ret;
}

static inline const char *_mpc_pipe_get_pipe_path(pipe_t *pipe)
{
    FORMULA_GUARD(pipe == NULL, NULL, ERR_INVALID_PTR);
    return (const char *)(pipe->path ? pipe->path : NULL);
}
