# How to use the DB?


## For Initialization
* **group.cfg**
	* For loading the group number for applicants of the DC/Step tracer
	```bash
	$ vim ./config/group.cfg
	```

* **pre_round_db.json**
	* The database file for the applicants from google docs
	```bash
	$ vim ./json/pre_round_db.json
	```

* Using the 'JsonParser' from json_parser.py, parsing the json and config file


## APIs(public methods)
* **For Player**
	* *before_player*
		* When inputted the key ***b***, you can get the player information at that time

	* *next_player*
		* When inputted the key ***n***, you can get the player information at that time

	* *current_player*
		* When you want to check the current palyer, you can get the player information at that time

* **For Groups**
	* *before_group*
		* When inputted the key ***f***, you can get the group and first player information at that time

	* *next_group*
		* When inputted the key ***g***, you can get the group and first player information at that time

	* *first_group*
		* You have to use this API when ***first loading time*** the GUI, for the draw the group and player information

* **For Management**
	* *manage_record*
		* params -> (player: dict, record: float, mode: str)
		* Get the **player** information through param and find player in the db
		* When matched the infromation both **player** and **DB**, will add/delete the record
		* The **mode** have to input "add" or "del" for the adding and deleting each record
		* The information of the **player** as below
		```bash
		player = {
			"name": "<string>",
			"affil": "<string>",
			"robot": "<string>"
		}
		```

	* *save_database*
		* params -> (path: str, mode: str)
		* For saving the database
		* The **path** is the saving directory the db
		* The **mode** have to input "pre" or "main" for the preliminary match and main match each

* **Return Type**
	* tuple

* **Return Data**
	* ***((User Information), [Group List], [User Records], [Rank in the Group])***

	* User Information (**tuple**)
		* Information about the current participant
		* *(name, affiliation, robot name)*

	* Group List (**list**)
		* The list of the participants in the group
		* *[[name, affiliation], [name, affiliation], ...]*

	* User Records (**list**)
		* Time records of the current participant
		* *[12.345, 23.456, ...]*

	* Rank in the Group (**list**)
		* The rank of all participants in the group
		* *[name, affiliation, top rank]*


## How to use?
* You have to ***import the preliminary_round_db_info.py*** and ***GetUserInfo class***
* And, can use to ***import the final_round_db_info.py and GetFinalUserInfo class***
* In the python files,
	```bash
	from final_round_db_info import GetFinalUserInfo
	from preliminary_round_db_info import GetUserInfo
	...
	...

	if __name__ == "__main__":
		USER = GetUserInfo(<path for db directory>, <path for preliminary db>)
		FINAL = GetFinalUserInfo(<path for db directory>, <path for final db>)

		USER.first_group()
		USER.next_group()
		USER.before_group()

		FINAL.first_group()
		FINAL.next_player()
		FINAL.before_player()
	```
