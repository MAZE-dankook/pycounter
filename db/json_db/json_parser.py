#!/usr/bin/env python3
"""This is the python file for parsing the json"""

import json
import os
import string
import sys

from config.config_parser import GetConfigInfo
from json_db.json_common_utils import (check_field, get_idx_in_group,
                                       set_group_info)

###############################################################################
# JsonParser class


class JsonParser:
    """Class for the parsing the json"""
    def __init__(self, cfg_path: str, json_path: str) -> None:
        self.__db_all = []
        self.__dct, self.__step, self.__fresh, self.__mouse = [], [], [], []

        self.__cur_idx = 0
        self.__parser_flag = False

        if set_group_info(cfg_path, self.__step, self.__dct) is False:
            return

        if self.__json_parsing(json_path) is False:
            return

        self.__parser_flag = True
        check_field(self.__db_all, self.__fresh, self.__step, self.__dct,
                    self.__mouse)

    def __del__(self) -> None:
        self.__parser_flag = False

    ###########################################################################
    # Public methods
    def dct_group(self, group: str) -> list:
        """Return DC Tracer Group"""
        idx = get_idx_in_group(group)
        if idx == -1 or idx >= len(self.__dct):
            return []

        return self.__dct[idx]

    def step_group(self, group: str) -> list:
        """Return Step Tracer Group"""
        idx = get_idx_in_group(group)
        if idx == -1 or idx >= len(self.__step):
            return []

        return self.__step[idx]

    @property
    def fresh(self) -> list:
        """Return Fresh Tracer list"""
        return self.__fresh

    @fresh.setter
    def fresh(self, new_list: list) -> None:
        """Set for the Fresh Tracer list"""
        self.__fresh = new_list

    @property
    def mouse(self) -> list:
        """Return Micro Mouse list"""
        return self.__mouse

    @property
    def db_all(self) -> list:
        """Return all database"""
        return self.__db_all

    ###########################################################################
    # Internal methods
    def __json_parsing(self, json_path: str) -> bool:
        if not os.path.exists(json_path):
            print("[ERR] Failed to find the file json path")
            return False

        with open(json_path) as json_file:
            self.__db_all = json.load(json_file)

        return True


def main():
    """main"""
    user = JsonParser("./config/group.cfg", "./json_db/pre_round_db.json")
    cfg = GetConfigInfo("./config/group.cfg")

    # Fresh Man
    print(user.fresh)

    # Step group
    for idx in range(0, cfg.get_step_group_num):
        print(user.step_group(string.ascii_uppercase[idx]))

    # DC group
    for idx in range(0, cfg.get_dc_group_num):
        print(user.dct_group(string.ascii_uppercase[idx]))

    # Micro Mouse
    print(user.mouse)


if __name__ == "__main__":
    sys.exit(main() or 0)
