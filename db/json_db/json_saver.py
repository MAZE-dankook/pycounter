#!/usr/bin/env python3
"""This is the python file for saving the json"""

import csv
import json
import sys

from json_db.json_common_utils import check_field, set_group_info

###############################################################################
# JsonSaver class


class JsonSaver:
    """Class for the saving the json"""

    CONFIG = "./config/group.cfg"

    def __init__(self) -> None:
        self.__save_db = []
        self.__dct, self.__step, self.__fresh, self.__mouse = [], [], [], []

        self.__init = False

    def __del__(self) -> None:
        self.__init = False

    ###########################################################################
    # Public methods
    def save_db_file(self, json_db: list, *, path: str, mode: str) -> bool:
        """Save database file"""
        self.__save_db = []
        if not ("fresh" or "step" or "dc" or "mouse" in mode):
            print("Need to input pre or main for the db saving!")
            return False

        # create the result database
        return self.__compute_result(json_db, path, mode)

    def print_db_file(self) -> bool:
        """Print the saved database"""
        print("\nTotal Participants : %u" % len(self.__save_db))

        for idx, user in enumerate(self.__save_db):
            print("\n-------------------------------------------------------")
            print("Participants Number \t: %d" % (idx + 1))
            print("Name \t\t\t: %s" % user["name"])
            print("Affiliation \t\t: %s" % user["affiliation"])
            print("Robot Name \t\t: %s" % user["robot"])
            print("Field \t\t\t: %s" % user["field"])
            print("Group \t\t\t: %s" % user["group"])
            print("Record \t\t\t: %s" % str(user["record"]))
            print("\n")
            return True
        return False

    ###########################################################################
    # Internal methods
    def __set_init_pre_match_info(self, json_db: list) -> bool:
        if self.__init is True:
            return True

        # set init data
        if set_group_info(JsonSaver.CONFIG, self.__step, self.__dct) is False:
            return False

        check_field(json_db, self.__fresh, self.__step, self.__dct,
                    self.__mouse)

        self.__init = True
        return True

    def __compute_result(self, json_db: list, path: str, mode: str) -> bool:
        # check the initial data
        if self.__set_init_pre_match_info(json_db) is False:
            return False

        group_t = {
            "fresh": self.__fresh,
            "Step": self.__step,
            "DC": self.__dct,
            "mouse": self.__mouse,
            "Step_Main": self.__step[0],
            "DC_Main": self.__dct[0],
        }

        if mode == "fresh" or mode == "mouse":
            rank = compute_rank(group_t[mode])
            save_csv(rank, path, mode)
        elif "DC" in mode or "Step" in mode:
            if mode == "DC_Main" or mode == "Step_Main":
                rank = compute_rank(group_t[mode])
                save_csv(rank, path, mode)
            else:
                rank = compute_rank_for_tournament(group_t[mode])
                save_final_round_db(rank, path, mode)
        else:
            return False

        self.__save_db = json_db
        return True


def compute_rank(group: list) -> list:
    """For computing the rank in the group"""
    rank = []
    for user in group:
        rlen = len(user["record"])
        if rlen:
            sort = sorted(user["record"])[0]
            rank.append(
                (user["name"], user["robot"], user["affiliation"], sort))

    rank.sort(key=lambda x: x[3])
    return rank


def compute_rank_for_tournament(group: list) -> list:
    """ For computing the rank for tournament """
    final = []
    wildcard = []

    for users in group:
        # get ranked list
        ret = compute_rank(users)
        for idx, user in enumerate(ret):
            if idx >= 2:
                wildcard.append(user)
            else:
                final.append(user)

    # checking wildcard users
    wildcard.sort(key=lambda x: x[3])

    # added wildcard users to final list
    final.extend(wildcard[0:2])

    # sort
    final.sort(key=lambda x: x[3], reverse=True)

    return final


def save_csv(sort_group: tuple, path: str, mode: str) -> None:
    """For saving the CSV file as the result of the match"""
    if path[-1] != "/":
        path += "/"

    with open(path + mode + "_result_db.csv", "w",
              encoding="utf-8") as csv_save:
        writer = csv.writer(csv_save)
        writer.writerow(("이름", "로봇명", "소속", "최고기록"))
        for user in sort_group:
            writer.writerow(user)

    print("Saved the DB in the " + path + mode + "_result_db.csv")


def save_final_round_db(final: list, path: str, mode: str) -> None:
    """For saving the json file for the final round"""
    if path[-1] != "/":
        path += "/"

    # update the information for fianl db
    new_db = []
    for user in final:
        temp = {}
        temp["field"] = f"Line Tracer({mode} Motor)"
        temp["name"] = user[0]
        temp["robot"] = user[1]
        temp["affiliation"] = user[2]
        temp["group"] = "A"
        temp["record"] = []

        new_db.append(temp)

    # save database
    with open(path + mode + "_final_round_db.json", "w",
              encoding="utf-8") as database_save:
        json.dump(new_db, database_save, indent=4, ensure_ascii=False)

    print("Saved the JSON in the " + path + mode + "_final_round_db.json")


def main():
    """main"""
    json_db = JsonSaver()
    json_db.save_db_file([], path="./", mode="fresh")
    json_db.print_db_file()


if __name__ == "__main__":
    sys.exit(main() or 0)
