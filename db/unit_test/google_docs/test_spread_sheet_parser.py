#!/usr/bin/env python3
# pylint: disable=too-many-function-args,redundant-unittest-assert,not-callable
"""This is the unittest file for the spread_sheet_parser"""

import unittest
import warnings

from google_docs.spread_sheet_parser import SpreadSheetParser

###############################################################################
# TestSpreadSheetParser class


class TestSpreadSheetParser(unittest.TestCase):
    """Class for the unittest"""

    JSON_PATH = "./google_docs/auth_key/auth_key_google_spread.json"

    def setUp(self) -> None:
        warnings.filterwarnings(action="ignore",
                                message="unclosed",
                                category=ResourceWarning)

        self.__class = SpreadSheetParser(TestSpreadSheetParser.JSON_PATH)

    def tearDown(self) -> None:
        if self.__class is not None:
            del self.__class

    ###########################################################################
    # Unittest methods
    def test_class(self) -> None:
        """Unit Test for the class"""
        with self.assertRaises(TypeError):
            SpreadSheetParser([], ())
            self.assertTrue(False)

    def test_parsing_spread_sheet(self) -> None:
        """ Unit Test for the parsing_spread_sheet """
        tc_1 = SpreadSheetParser()
        self.assertListEqual(tc_1.parsing_spread_sheet(), [])

        self.assertTrue("부문" in self.__class.parsing_spread_sheet()[0])
        self.assertTrue("성함" in self.__class.parsing_spread_sheet()[0])
        self.assertTrue("소속" in self.__class.parsing_spread_sheet()[0])
        self.assertTrue("로봇명" in self.__class.parsing_spread_sheet()[0])
        self.assertTrue("CPU" in self.__class.parsing_spread_sheet()[0])

    def test_raw_db(self) -> None:
        """ Unit Test for the raw_db """
        tc_1 = SpreadSheetParser()
        self.assertListEqual(tc_1.raw_db, [])

        with self.assertRaises(TypeError):
            print(tc_1.raw_db())

        self.assertListEqual(self.__class.raw_db, [])

        self.__class.parsing_spread_sheet()
        self.assertNotEqual(self.__class.raw_db, [])

    def test_save_db(self) -> None:
        """ Unit Test for the save_db """
        tc_1 = SpreadSheetParser()
        self.assertEqual(tc_1.save_db(), False)

        self.assertEqual(self.__class.save_db(), False)

        self.__class.parsing_spread_sheet()
        self.assertEqual(self.__class.save_db(), False)


if __name__ == "__main__":
    unittest.main()
