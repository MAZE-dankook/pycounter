#!/usr/bin/env python3
"""This is the unittest file for the json_common_utils"""

import unittest

from config.config_parser import GetConfigInfo
from json_db.json_common_utils import (check_field, get_idx_in_group,
                                       set_group_info)
from json_db.json_parser import JsonParser

###############################################################################
# TestJsonCommonUtils class


class TestJsonCommonUtils(unittest.TestCase):
    """Class for the unittest"""
    def setUp(self) -> None:
        pass

    def tearDown(self) -> None:
        pass

    ###########################################################################
    # Unittest methods
    def test_set_group_info(self) -> None:
        """Unit Test for the set_group_info"""
        self.assertFalse(set_group_info("", [], []))

        dct = []
        step = []
        self.assertTrue(set_group_info("./config/group.cfg", step, dct))

        cfg = GetConfigInfo("./config/group.cfg")
        self.assertEqual(cfg.get_dc_group_num, len(dct))
        self.assertEqual(cfg.get_step_group_num, len(step))

    def test_check_field(self) -> None:
        """Unit Test for the check_field"""
        data, fresh, step, dct, mouse = [], [], [], [], []
        check_field(data, fresh, step, dct, mouse)
        self.assertListEqual(fresh, [])
        self.assertListEqual(step, [])
        self.assertListEqual(dct, [])
        self.assertListEqual(mouse, [])

        data = JsonParser("./config/group.cfg", "./json_db/pre_round_db.json")
        set_group_info("./config/group.cfg", step, dct)
        check_field(data.db_all, fresh, step, dct, mouse)
        self.assertEqual(len(fresh), 4)
        self.assertEqual(len(step), 7)
        self.assertEqual(len(dct), 3)
        self.assertEqual(len(mouse), 4)

    def test_get_idx_in_group(self) -> None:
        """Unit Test for the get_idx_in_group"""
        self.assertEqual(get_idx_in_group(""), -1)
        self.assertEqual(get_idx_in_group("Test"), -1)
        self.assertEqual(get_idx_in_group([]), -1)
        self.assertEqual(get_idx_in_group("A"), 0)
        self.assertEqual(get_idx_in_group("B"), 1)
        self.assertEqual(get_idx_in_group("C"), 2)
        self.assertEqual(get_idx_in_group("D"), 3)


if __name__ == "__main__":
    unittest.main()
