#!/usr/bin/env python3
# pylint: disable=too-many-function-args, not-callable
"""This is the unittest file for the json_parser"""

import unittest

from json_db.json_parser import JsonParser

###############################################################################
# TestJsonParser class


class TestJsonParser(unittest.TestCase):
    """Class for the unittest"""
    def setUp(self) -> None:
        self.__json = JsonParser("./config/group.cfg",
                                 "./json_db/pre_round_db.json")

    def tearDown(self) -> None:
        pass

    ###########################################################################
    # Unittest methods
    def test_class(self) -> None:
        """Unit Test for the class"""
        tc_1 = JsonParser("", "")
        self.assertListEqual(tc_1.dct_group("A"), [])
        self.assertListEqual(tc_1.step_group("A"), [])
        self.assertListEqual(tc_1.fresh, [])
        self.assertListEqual(tc_1.mouse, [])
        self.assertListEqual(tc_1.db_all, [])

        with self.assertRaises(TypeError):
            JsonParser([], (), "")

        with self.assertRaises(TypeError):
            JsonParser((), [])

    def test_dct_group(self) -> None:
        """Unit Test for the dct_group"""
        self.assertListEqual(self.__json.dct_group(""), [])
        self.assertListEqual(self.__json.dct_group("D"), [])

        self.assertEqual(len(self.__json.dct_group("A")), 4)
        self.assertEqual(len(self.__json.dct_group("B")), 3)
        self.assertEqual(len(self.__json.dct_group("C")), 4)

    def test_step_group(self) -> None:
        """Unit Test for the step_group"""
        self.assertListEqual(self.__json.step_group(""), [])
        self.assertListEqual(self.__json.step_group("H"), [])

        self.assertEqual(len(self.__json.step_group("A")), 3)
        self.assertEqual(len(self.__json.step_group("B")), 3)
        self.assertEqual(len(self.__json.step_group("C")), 4)
        self.assertEqual(len(self.__json.step_group("D")), 3)
        self.assertEqual(len(self.__json.step_group("E")), 3)
        self.assertEqual(len(self.__json.step_group("F")), 3)
        self.assertEqual(len(self.__json.step_group("G")), 2)

    def test_fresh(self) -> None:
        """Unit Test for the fresh"""
        self.assertEqual(len(self.__json.fresh), 4)
        self.assertIsInstance(self.__json.fresh, list)

        with self.assertRaises(TypeError):
            self.__json.fresh()

    def test_mouse(self) -> None:
        """Unit Test for the mouse"""
        self.assertEqual(len(self.__json.mouse), 4)
        self.assertIsInstance(self.__json.mouse, list)

        with self.assertRaises(TypeError):
            self.__json.mouse()

    def test_db_all(self) -> None:
        """Unit Test for the db_all"""
        self.assertEqual(len(self.__json.db_all), 40)
        self.assertIsInstance(self.__json.db_all, list)

        with self.assertRaises(TypeError):
            self.__json.db_all()


if __name__ == "__main__":
    unittest.main()
