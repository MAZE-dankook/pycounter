#!/usr/bin/env python3
# pylint: disable=no-value-for-parameter, redundant-unittest-assert
# pylint: disable=too-many-function-args
"""This is the unittest file for the config_parser"""

import unittest

from config.config_parser import GetConfigInfo

###############################################################################
# TestGetConfigInfo class


class TestGetConfigInfo(unittest.TestCase):
    """Class for the unittest"""
    def setUp(self) -> None:
        self.__class = GetConfigInfo("./config/group.cfg")

    def tearDown(self) -> None:
        del self.__class

    ###########################################################################
    # Unittest methods

    def test_class(self) -> None:
        """Unit Test for the class"""
        # input the int parameter
        tc_1 = GetConfigInfo(1524)
        self.assertEqual(tc_1.get_dc_group_num, -1)
        self.assertEqual(tc_1.get_step_group_num, -1)

        try:
            # input the empty params
            tc_2 = GetConfigInfo()
            self.assertEqual(tc_2.get_dc_group_num, -1)
            self.assertTrue(False)
        except TypeError:
            self.assertTrue(True)

        try:
            # input the wrong params
            tc_3 = GetConfigInfo([], ())
            self.assertEqual(tc_3.get_dc_group_num, -1)
            self.assertTrue(False)
        except TypeError:
            self.assertTrue(True)

    def test_get_dc_group_num(self) -> None:
        """Unit Test for the get_dc_group_num"""
        self.assertEqual(self.__class.get_dc_group_num, 3)

    def test_get_step_group_num(self) -> None:
        """Unit Test for the get_step_group_num"""
        self.assertEqual(self.__class.get_step_group_num, 7)


if __name__ == "__main__":
    unittest.main()
