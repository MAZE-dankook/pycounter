#!/usr/bin/env python3
# pylint: disable=too-many-instance-attributes
"""This is the unittest file for the preliminary_round_db_info"""

import os
import unittest

from preliminary_round_db_info import GetUserInfo

###############################################################################
# TestGetUserInfo class


class TestGetUserInfo(unittest.TestCase):
    """Class for the unittest"""
    def setUp(self) -> None:
        self.__user = GetUserInfo("./", "json_db/pre_round_db.json")

    def tearDown(self) -> None:
        pass

    ###########################################################################
    # Unittest methods
    def test_class(self) -> None:
        """Unit Test for the class"""
        tc_1 = GetUserInfo([], ())
        tc_2 = GetUserInfo("", "")

        self.assertIsInstance(tc_1, GetUserInfo)
        self.assertIsInstance(tc_2, GetUserInfo)

    def test_before_player(self) -> None:
        """Unit Test for the before_player"""
        self.__check_player(self.__user.before_player)

    def test_next_player(self) -> None:
        """Unit Test for the next_player"""
        self.__check_player(self.__user.next_player)

    def test_current_player(self) -> None:
        """Unit Test for the current_player"""
        self.__check_player(self.__user.current_player)

    def test_before_group(self) -> None:
        """Unit Test for the before_group"""
        tc_1 = GetUserInfo("", "")
        self.__check_group(tc_1.before_group, self.__user.before_group)

    def test_next_group(self) -> None:
        """Unit Test for the next_group"""
        tc_1 = GetUserInfo("", "")
        self.__check_group(tc_1.next_group, self.__user.next_group)

    def test_first_group(self) -> None:
        """Unit Test for the first_group"""
        tc_1 = GetUserInfo("", "")
        self.__check_group(tc_1.first_group, self.__user.first_group)

    def test_manage_record(self) -> None:
        """Unit Test for the manage_record"""
        player = dict()

        tc_1 = GetUserInfo("", "")
        self.assertFalse(tc_1.manage_record(player, 0.0, "abc"))
        self.assertFalse(self.__user.manage_record(player, 0.0, "abc"))
        self.assertFalse(self.__user.manage_record(player, 0.0, "del"))

        player["name"] = "이재성"
        player["affiliation"] = "Intel Korea Ltd"
        player["robot"] = "_varhae_(발해)"
        self.assertTrue(self.__user.manage_record(player, 0.0, "add"))
        self.assertTrue(self.__user.manage_record(player, 0.0, "del"))
        self.assertFalse(self.__user.manage_record(player, 0.0, "del"))

    def test_save_database(self) -> None:
        """Unit Test for the save_database"""
        tc_1 = GetUserInfo("", "")
        self.assertFalse(tc_1.save_database("./", "mouse"))

        self.__user.save_database("./", "mouse")
        self.assertTrue(os.path.exists("./mouse_result_db.csv"))
        os.remove("./mouse_result_db.csv")

    ###########################################################################
    # Internal methods

    def __check_player(self, method: classmethod) -> None:
        self.assertTupleEqual(method(), ())

        self.__user.first_group()
        self.assertNotEqual(len(method()), 0)

    def __check_group(self, new_func: classmethod,
                      pre_func: classmethod) -> None:
        self.assertTupleEqual(new_func(), ())
        self.assertNotEqual(len(pre_func()), 0)


if __name__ == "__main__":
    unittest.main()
