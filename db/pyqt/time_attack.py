#!/usr/bin/env python3
"""This is the python file for creating the database of the Time Attack"""

import json
import os
import sys

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QFileDialog, QMainWindow, QMessageBox

###############################################################################
# TimeAttack class

CUSTOM_UI = uic.loadUiType("time_attack.ui")[0]


class TimeAttack(QMainWindow, CUSTOM_UI):
    """Class for the time attack"""
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        # set title
        self.setWindowTitle("Time Attack!")

        # set GUI
        self.file_open.clicked.connect(self.file_open_clicked)
        self.add_button.clicked.connect(self.add_button_clicked)
        self.del_button.clicked.connect(self.del_button_clicked)
        self.save_button.clicked.connect(self.save_button_clicked)

        # internal variables
        self.__db_path = None
        self.__db_all = None

    def __del__(self):
        del self.__db_all

    ###########################################################################
    # Public methods
    def file_open_clicked(self) -> None:
        """When file open button clicked,"""
        self.__db_path = QFileDialog.getOpenFileName()[0]

        # get json db
        if self.__check_format(self.__db_path[-4:]) is True:
            # set text
            self.label.setText(self.__db_path)

            # get json
            self.__get_json_db(self.__db_path)

            # load to combobox
            self.__add_user_on_ui()

    def add_button_clicked(self) -> None:
        """When add button clicked,"""
        # empty combobox
        if self.user_combobox.count() == 0:
            return

        # get user information from combobox
        user = self.user_combobox.currentText()

        # add to the list widget
        self.list_widget.addItem(user)

        # delete on the combobox
        self.user_combobox.removeItem(self.user_combobox.currentIndex())

    def del_button_clicked(self) -> None:
        """When del button clicked,"""
        # empty list widget
        if self.list_widget.count() == 0:
            return

        # not selected the list
        cur_item = self.list_widget.currentItem()
        if cur_item is None:
            return

        # add to the combobox
        self.user_combobox.addItem(cur_item.text())
        self.user_combobox.model().sort(0)

        # delete on the list widget
        self.list_widget.takeItem(self.list_widget.currentRow())

    def save_button_clicked(self) -> None:
        """When save button clicked,"""
        if self.__db_path is None:
            return

        # get data from list widget
        count = self.list_widget.count()
        if count == 0:
            return

        raw_data = []
        for i in range(count):
            raw_data.append(self.list_widget.item(i).text())

        # make the time attack database
        time_attack_db = []
        for user in raw_data:
            idx = int(user.split(",")[0])
            time_attack_db.append(self.__db_all[idx])

        # save database
        path = os.path.dirname(self.__db_path)
        with open(path + "/time_attack.json", "w") as save_db:
            json.dump(time_attack_db, save_db, indent=4, ensure_ascii=False)

        # run the popup
        QMessageBox.about(self, "Popup", "Saved")

    ###########################################################################
    # Internal methods
    def __check_format(self, file_format: str) -> bool:
        if file_format != "json":
            self.label.setText("Only select the json file format!")
            self.__db_all, self.__db_path = None, None
            self.user_combobox.clear()
            return False

        return True

    def __get_json_db(self, path: str) -> None:
        with open(path) as read_file:
            self.__db_all = json.load(read_file)

    def __add_user_on_ui(self) -> None:
        for idx, user in enumerate(self.__db_all):
            field = user["field"]
            name = user["name"]
            affil = user["affiliation"]
            robot = user["robot"]

            if "Mouse" in field:
                continue

            info = str(idx) + ", " + field + ", "
            info += name + ", " + affil + ", " + robot
            self.user_combobox.addItem(info)


def main() -> None:
    """main"""
    app = QApplication(sys.argv)
    t_attack = TimeAttack()
    t_attack.show()
    app.exec_()


if __name__ == "__main__":
    sys.exit(main() or 0)
