#!/bin/bash

ROOT_DIR=$(git rev-parse --show-toplevel)

source "${ROOT_DIR}/db/.profile"

RET="$?"
if [[ "${RET}" -ne "0" ]]
then
	echo "Failed to set envrionment!"
	exit 1
fi
echo "[json_db] Success to set envrionment for python"

cd "${ROOT_DIR}/db/pyqt" || exit 1
python3 set_group.py
