#!/usr/bin/env python3
"""This is the python file for saving the config"""

import os
import sys
from configparser import ConfigParser, MissingSectionHeaderError
from subprocess import call

###############################################################################
# SetConfigInfo class


class SetConfigInfo:
    """Class for the saving the config file"""
    def __init__(self, path: str) -> None:
        try:
            if not os.path.exists(path):
                self.__cfg = None
                return

            self.__path = path
            self.__cfg = ConfigParser()
            self.__cfg.read(path)
        except MissingSectionHeaderError:
            print("Failed to set the self.__cfg in the SetConfigInfo")
            if self.__cfg:
                del self.__cfg
                self.__cfg = None

    ###########################################################################
    # Public methods
    def save_config_file(self, *, dc_num: int, step_num: int) -> bool:
        """Save Config File"""
        if self.__has_cfg() is False:
            return False

        if self.__set_config_value(dc_num, step_num) is False:
            return False

        with open(self.__path, "w") as save_cfg:
            self.__cfg.write(save_cfg)

        # remove trailing lines
        call(["sed", "-i", "$d", self.__path])
        return True

    def get_config_value(self) -> tuple:
        """Get the config value"""
        if self.__has_cfg() is False:
            return (-1, -1)

        if self.__cfg.has_section("group") is False:
            print("Failed to load section information from the config file.")
            return (-1, -1)

        try:
            return self.__cfg["group"]["dc"], self.__cfg["group"]["step"]
        except KeyError:
            print("Failed to load key value from the config file")
            return (-1, -1)

    ###########################################################################
    # Internal methods
    def __has_cfg(self) -> bool:
        return False if self.__cfg is None else True

    def __set_config_value(self, dc_num: int, step_num: int) -> bool:
        """Set the group number in the DC/Step"""
        if dc_num <= 0 or step_num <= 0:
            print("Failed to save the config file.")
            return False

        if self.__cfg.has_section("group") is False:
            self.__cfg.add_section("group")

        self.__cfg.set("group", "dc", str(dc_num))
        self.__cfg.set("group", "step", str(step_num))
        return True


def main():
    """main"""
    save_cfg = SetConfigInfo("./group.cfg")
    save_cfg.save_config_file(dc_num=3, step_num=3)
    print(save_cfg.get_config_value())


if __name__ == "__main__":
    sys.exit(main() or 0)
