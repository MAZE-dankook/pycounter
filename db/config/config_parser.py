#!/usr/bin/env python3
"""This is the python file for parsing the config"""

import sys
from configparser import ConfigParser, NoOptionError, NoSectionError

###############################################################################
# GetConfigInfo class


class GetConfigInfo:
    """Class for the parsing the config file"""
    def __init__(self, path: str):
        self.__cfg = None
        try:
            self.__cfg = ConfigParser()

            self.__cfg.read(path)
            self.__get_dc_group_num = int(self.__cfg.get("group", "dc"))
            self.__get_step_group_num = int(self.__cfg.get("group", "step"))
            self.__cfg_flag = True

        except (NoOptionError, NoSectionError, TypeError):
            self.__cfg_flag = False
            if self.__cfg:
                del self.__cfg
                self.__cfg = None

    ###########################################################################
    # Public methods
    @property
    def get_dc_group_num(self) -> int:
        """Return DC Group Number"""
        return self.__get_dc_group_num if self.__cfg_flag is True else -1

    @property
    def get_step_group_num(self) -> int:
        """Return Step Group Number"""
        return self.__get_step_group_num if self.__cfg_flag is True else -1


def main():
    """main"""
    cfg_info = GetConfigInfo("group.cfg")
    print(cfg_info.get_dc_group_num)
    print(cfg_info.get_step_group_num)


if __name__ == "__main__":
    sys.exit(main() or 0)
