#!/usr/bin/env python3
"""This is the python file for parsing the google spread sheet"""

import json
import os
import sys

import gspread
from google.auth import exceptions
from oauth2client.service_account import ServiceAccountCredentials

###############################################################################
# SpreadSheetParser class


class SpreadSheetParser:
    """Class for the parsing the Google spread sheet"""

    DEFAULT_JSON = "auth_key/auth_key_google_spread.json"

    def __init__(self, json_path: str = DEFAULT_JSON) -> None:
        self.__init_done = False
        if not os.path.exists(json_path):
            return

        # need to input from GUI
        self.__jp = json_path
        self.__title = "2020 Micro Robot Contest"
        self.__sheet = "sheet_1"

        self.__raw_db = []
        self.__save_db = []
        self.__get_flag = False
        self.__init_done = True

    def __del__(self) -> None:
        self.__get_flag = False
        self.__init_done = False

    ###########################################################################
    # Public methods
    def parsing_spread_sheet(self) -> list:
        """Get data from google spread sheet and return"""
        if self.__init_done is False or self.__get_spread_sheet() is False:
            return []

        return self.raw_db

    @property
    def raw_db(self) -> list:
        """Return all of the raw db"""
        return [] if self.__init_done is False else self.__raw_db

    def save_db(self) -> bool:
        """Save the db to the json"""
        if self.__init_done is False or self.__get_flag is False:
            return False

        # get the path for the json
        cur_path = os.getcwd()
        base_path = os.path.basename(cur_path)
        json_path = cur_path[:-len(base_path)] + "json_db/pre_round_db.json"
        print(json_path)
        if not os.path.exists(json_path):
            return False

        # change the final database
        self.__raw_db_to_json()

        # save the db to the json
        with open(json_path, "w") as json_file:
            json.dump(self.__save_db, json_file, indent=4, ensure_ascii=False)

        print("Save the pre_round_db.json file in the {0}".format(json_path))
        return True

    ###########################################################################
    # Internal methods
    def __get_spread_sheet(self) -> bool:
        scope = [
            "https://spreadsheets.google.com/feeds",
            "https://www.googleapis.com/auth/drive",
        ]

        try:
            credentials = ServiceAccountCredentials.from_json_keyfile_name(
                self.__jp, scope)
            spread = gspread.authorize(credentials)
            raw_data = spread.open(self.__title).worksheet(self.__sheet)

            self.__raw_db = raw_data.get_all_values()
            self.__get_flag = True
            return True
        except FileNotFoundError:
            return False
        except exceptions.RefreshError:
            print("Failed to check the token!")
            return False
        except exceptions.TransportError:
            print("Failed to connect to the Google Spread Sheet!")
            return False

    def __raw_db_to_json(self) -> None:
        self.__save_db = []
        table = ["", "field", "name", "affiliation", "robot"]

        for user_idx, user in enumerate(self.__raw_db):
            if user_idx == 0:
                continue

            temp = {}
            for idx in range(1, 5):
                temp[table[idx]] = user[idx]

            temp["group"] = "A"
            temp["record"] = []

            self.__save_db.append(temp)


def main():
    """main"""
    gss = SpreadSheetParser()
    gss.parsing_spread_sheet()
    gss.save_db()


if __name__ == "__main__":
    sys.exit(main() or 0)
